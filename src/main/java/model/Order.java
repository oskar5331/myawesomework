package model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private static final Long FIRST_ID = 1L;
    private static Long idCount = FIRST_ID;



    private Long id;
    private String orderNumber;
    private List<OrderRow> orderRows;
    public Order(){};
    public Order(String orderNumber) {
        this(null, orderNumber);
    }
    public Order(Long id, String orderNumber) { this(id, orderNumber, null); }
    public Order(Long id, String orderNumber, List<OrderRow> orderRows) {
        this.id = id;
        this.orderNumber = orderNumber;
        this.orderRows = orderRows;
    }

    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty
    public Long getID() {
        return id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) { this.orderNumber = orderNumber; }

    public List<OrderRow> getOrderRows() {
        return orderRows;
    }

    public void setOrderRows(List<OrderRow> orderRows) {
        this.orderRows = orderRows;
    }

    public boolean anyOrderRows() {
        return (orderRows != null)
                ? true
                : false;

    }

    public void addOrderRow(OrderRow orderRow) {
        if (anyOrderRows())
            orderRows.add(orderRow);
        else {
            orderRows = new ArrayList<OrderRow>();
            orderRows.add(orderRow);
        }

    }
}
