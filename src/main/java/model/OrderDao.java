package model;

import util.DataSourceProvider;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDao {


    public Order insertOrder(Order order) {

        String sqlOrders = "insert into ORDERS (id, ordernumber) " +
                "values (next value for seq1, ?)";
        String sqlOrderrow = "insert into ORDERROW (id, itemname, quantity, price) " +
                "values (?, ?, ?, ?)";

        //gets connection from the pool
        DataSource dataSource = DataSourceProvider.getDataSource(); // max 3 connections


        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlOrders, new String[] {"id"})) {

            //prepares string for database update
            ps.setString(1, order.getOrderNumber());

            //updates database
            ps.executeUpdate();

            //gets back list of items asked from preparestatement
            ResultSet rs = ps.getGeneratedKeys();

            rs.next();
            String orderID = rs.getString("id");

            if (order.anyOrderRows()) {
                try (PreparedStatement ps2 = conn.prepareStatement(sqlOrderrow)) {
                    for (OrderRow orderRow:order.getOrderRows()) {
                        ps2.setString(1, orderID);
                        ps2.setString(2, orderRow.getItemName());
                        ps2.setString(3, orderRow.getQuantity());
                        ps2.setString(4, orderRow.getPrice());
                        ps2.executeUpdate();
                    }
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

            return new Order(Long.parseLong(orderID), order.getOrderNumber(), order.getOrderRows());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public Order getOrderById(String id) {

        Order newOrder = new Order();

        String sql = "select id, ordernumber, itemname, quantity, price from  ORDERS\n" +
                "  left join ORDERROW\n" +
                "  on ORDERS.id = ORDERROW.id\n" +
                "  where id = ?";

        //gets connection from the pool
        DataSource dataSource = DataSourceProvider.getDataSource(); // max 3 connections


        try (Connection conn = dataSource.getConnection();
             //makes instance which can prepare string for database update
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //prepares string for database update
            ps.setString(1, id);

            //querys database
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                newOrder.setId(rs.getLong("id"));
                newOrder.setOrderNumber(rs.getString("ordernumber"));
                if ((rs.getString("itemname") != null) &&
                        (rs.getString("quantity") != null) &&
                        (rs.getString("price") != null)) {
                    newOrder.addOrderRow(new OrderRow(
                            rs.getString("itemname"),
                            rs.getString("quantity"),
                            rs.getString("price")
                    ));
                    while (rs.next())
                        newOrder.addOrderRow(new OrderRow(
                                rs.getString("itemname"),
                                rs.getString("quantity"),
                                rs.getString("price")
                        ));
                    //System.out.println(rs.getString("itemname"));
                    }
                return newOrder;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }


    public List<Order> getOrdersList() {

        List<Order> orderList = new ArrayList<>();

        String sql = "select * from ORDERS";

        //gets connection from the pool
        DataSource dataSource = DataSourceProvider.getDataSource(); // max 3 connections


        try (Connection conn = dataSource.getConnection();
             //makes instance what can get data from database
             Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                orderList.add(new Order(rs.getLong("id"),
                        rs.getString("ordernumber")));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return orderList;
    }

    public void deleteOrder(String id) {
        String deleteOrdersSql = "delete from ORDERS where id = ?";
        String deleteOrderrowSql = "delete from ORDERROW where id = ?";

        DataSource dataSource = DataSourceProvider.getDataSource();

        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(deleteOrdersSql);
            ps.setString(1, id);
            ps.executeUpdate();

            PreparedStatement ps2 = conn.prepareStatement(deleteOrderrowSql);
            ps2.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
