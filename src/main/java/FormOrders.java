import model.OrderDao;
import util.Util;
import model.Order;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("orders/form")
public class FormOrders extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setHeader("Content-Type", "application/json");

        Order order = new Order(Util.findParam(Util.readStream(request.getInputStream()), "orderNumber"));

        order = new OrderDao().insertOrder(order);

        response.getWriter().print(order.getID().toString());

    }
}
