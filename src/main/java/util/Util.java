package util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class Util {

    public static String readStream(InputStream is) {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(is));

        return buffer.lines().collect(Collectors.joining("\n"));
    }

    public static String findParam(String input, String lookFor){
        if (!input.isEmpty()) {
            String[] splitInput = input.split("=");
            for (int i = splitInput.length - 1; i >= 0; i--){
                splitInput[i] = splitInput[i].trim();
                //System.out.println(splitInput[i]);
                if (splitInput[i].matches(lookFor)) {
                    //System.out.println(5534);
                    return splitInput[i + 1];
                }
            }
        }
        return null;
    }
/**
    public static String[][] dataForJson (Order order) {
        String[][] dataArray = {{order.getIdStr(), order.getID()}, {order.getIdStr(), order.getID()}};

        return dataArray;
    }
**/

/*
    public static String jsonGenerator(Order order) {
        String json = "{ \"" + order.getIdStr() + "\": \"" + order.getID() + "\", \"" +
                order.getOrderNumberString() + "\": \"" + order.getOrderNumber() + "\" }";

        return json;

    }
   */

}
