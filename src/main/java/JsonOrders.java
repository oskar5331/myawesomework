

import model.OrderDao;
import util.Util;
import model.Order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebServlet("api/orders")
public class JsonOrders extends HttpServlet{
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setHeader("Content-Type", "application/json");

        Enumeration<String> params = request.getParameterNames();

        if (params.hasMoreElements()) {
            while (params.hasMoreElements()) {
                if (params.nextElement().equals("id")) {
                    response.getWriter().print(
                            new ObjectMapper().writeValueAsString(new OrderDao().getOrderById(
                                    request.getParameter("id")
                            ))
                    );
                }
            }
        } else {
            List<Order> ordersList = new OrderDao().getOrdersList();
            List<String> jsonList = new ArrayList<>();
            for (Order order:ordersList) {
                jsonList.add(new ObjectMapper().writeValueAsString(order));
            }
            response.getWriter().print(jsonList);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setHeader("Content-Type", "application/json");

        Order order = new ObjectMapper().readValue(Util.readStream(request.getInputStream()), Order.class);

        order = new OrderDao().insertOrder(order);

        response.getWriter().print(new ObjectMapper().writeValueAsString(order));
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Enumeration<String> params = request.getParameterNames();

        if (params.hasMoreElements()) {
            while (params.hasMoreElements()) {
                if (params.nextElement().equals("id")) {
                    new OrderDao().deleteOrder(request.getParameter("id"));
                }
            }
        }
    }
}


