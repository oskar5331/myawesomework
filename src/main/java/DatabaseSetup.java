import util.DataSourceProvider;
import util.FileUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@WebListener()
public class DatabaseSetup implements ServletContextListener {

    private String URL = "jdbc:hsqldb:mem:my-database";
    //private String URL = "jdbc:hsqldb:file:${user.home}/data/jdbc/db;shutdown=true";


    public DatabaseSetup() {
    }

    public void contextInitialized(ServletContextEvent sce) {

        DataSourceProvider.setDbUrl(URL);
        DataSource dataSource = DataSourceProvider.getDataSource();

        try (Connection conn = dataSource.getConnection();
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(FileUtil.readFileFromClasspath("schema.sql"));

        } catch (SQLException e) {
            System.out.println("connection error.");
            throw new RuntimeException(e);
        }
        System.out.println("connection done.");
    }

    public void contextDestroyed(ServletContextEvent sce) {

    }


}
